import React, { Component } from 'react'
import ItemShoe from './ItemShoe'

export default class ListShoe extends Component {
    renderListShoe = ()=>{
       return this.props.shoeArr.map((item,index)=>{
        return <ItemShoe
        handleDetailShoe = {this.props.handleDetailShoe}
        handleAddToCart = {this.props.handleAddToCart} 
        key={index} 
        data={item}/>
       })
    }
    render() {
    return (
      <div className='row container mx-auto'>
        {this.renderListShoe()}</div>
    )
  }
}
