import React, { Component } from "react";

export default class ItemShoe extends Component {
 
    render() {
        console.log(this.props);
        let {image,name,price,id}= this.props.data
    return (
      <div className="col-3 p-1">
         <div className="card text-left h-100">
          <img className="card-img-top" src={image} alt />
          <div className="card-body">
            <h5 className="card-title">{name}</h5>
            <h2 className="card-text">{price} $</h2>
          </div>
          <div>
          <button onClick={()=>{
            this.props.handleDetailShoe(this.props.data)}} className="btn btn-secondary">For More Detail</button>
        <button onClick={()=>{
          this.props.handleAddToCart(this.props.data)
        }} className="btn btn-danger">Add to Cart</button>
        </div>
        </div>
      </div>
    );
  }
}
