import React, { Component } from "react";

export default class Cart extends Component {
  renderTbody = () => {
    return this.props.cart.map((item) => {
      return (
        <tr>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>
            <img src={item.image} style={{ width: "50px" }} alt="" />
          </td>
          <td>{item.price}</td>

          <td>
            <button onClick={()=>{{this.props.decreaseAmount(item)}}} className="btn btn-danger">-</button>
            <span className="p-2">{item.number}</span>
            <button onClick={()=>{{this.props.increaseAmount(item)}}} className="btn btn-success">+</button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div>
        <table className="table">
          <thead>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Img</th>
              <th>Price</th>
              <th>Quantity</th>
            </tr>
          </thead>
          <tbody>{this.renderTbody()}</tbody>
        </table>
      </div>
    );
  }
}
